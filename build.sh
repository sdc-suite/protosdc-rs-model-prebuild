#!/bin/bash

echo "Cloning source repository with branch $SOURCE_BRANCH"
git clone -b "$SOURCE_BRANCH" https://gitlab.com/sdc-suite/proto-model.git || exit 10

echo "Building proto-model"
cd proto-model || exit 20
./gradlew executeConverter executeConverterExtensions copyProto || exit 30

echo "Copying the rust model outside of the source repository"
cd .. || exit 50
cp -Lr proto-model/rust rust-model || exit 60

echo "Initializing the git repository"
cd rust-model || exit 70
git init || exit 80
git add -A || exit 85

TARGET_BRANCH=build_"$SOURCE_BRANCH"_"$CI_PIPELINE_IID"
echo "Pushing to repository $TARGET_BRANCH at https://$CI_SERVER_HOST/$CI_PROJECT_PATH.git"
git config user.name "CI" || exit 90
git config user.email "CI@$CI_SERVER_HOST" || exit 100
git commit -a -m "Build $SOURCE_BRANCH" || exit 110
# if in doubt, force push to override any changes
git push --force "https://ci_token:$CI_PUSH_TOKEN@$CI_SERVER_HOST/$CI_PROJECT_PATH.git" HEAD:"$TARGET_BRANCH" || exit 120